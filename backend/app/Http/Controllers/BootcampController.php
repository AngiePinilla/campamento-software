<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamp;
use App\Http\Requests\StoreBootcampRequest;
 use App\Http\Resources\BootcampCollection;
use App\Http\Resources\BootcampResource;
use App\Http\Controllers\BaseController;

class BootcampController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Metodo json: 
        //Parametros: 1.data a enviar el servidor
        //2. Codigo de estatus http
        //return response()->json( [new BootcampCollection(Bootcamp::all()) ] ,200);

        try{

        return $this->sendResponse(new BootcampCollection(Bootcamp::all()));

        }catch(\Exception $e){
            return $this->sendError('Server Error',500);
        }

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBootcampRequest $request){
        // //1.establecer reglas de validacion
        // $reglas=[
        //     "name"=>"required",
        //     "description"=>"required",
        //     "websie"=>"required",
        //     "phone"=>"required",
        //     "user_id"=>"required",
        //     "average_rating"=>"required",
        //     "average_cost"=>"required"
        // ];

        // //2.validador
        // $v=Validator::make($request->all(),$reglas);

        // //3.Validar
        // if($v->fails()){
        //     //responde de error
        //     return response()->json(["success"=>false,"errors"=>$v->errors()],422);
        // }
        //1. Traer el payload
        // $datos=$request->all();
        //2. Crear el nuevo bootcamp
        // Bootcamp::create($datos);
        $p=Bootcamp::create($request->all());
        /*$p->name = $request->name;
        $p->description = $request->description;
        $p->websie = $request->websie;
        $p->phone = $request->phone;
        $p->average = $request->name;
        $p->name = $request->name;
        $p->name = $request->name;*/
        try {
            

        //return response()->json( [ "success"=>true,"data"=>new BootcampResource (Bootcamp::create($request->all())) ] ,201);

        return $this->sendResponse(new BootcampResource ($p),201);

        }catch (\Exception $e) {

            return $this->sendError('Server Error',500);
    
            }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)    {

        try {
            //1. encontrar el bootcamp
        $bootcamp = Bootcamp::find($id);
        //2. en caso de que no este
        if (!$bootcamp) {
            return $this->sendError("bootcamp con id:$id not existe",400);
        }
        
        return $this->sendResponse(new BootcampResource ($bootcamp));
        
        } catch (\Exception $e) {

        return $this->sendError('Server Error',500);

        }
        
        
        //return response()->json( [ "success"=>true,"data"=>new BootcampResource (Bootcamp::find($id)) ] ,200);
        
        //return Bootcamp::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        try {
             //1.Localizar el bootcamp
        $b = Bootcamp::find($id);

        if (!$b) {
            return $this->sendError("bootcamp con id:$id not existe",400);
        }

        $b->update($request->all());
        //2. Crear el nuevo bootcamp

        return $this->sendResponse(new BootcampResource($b));

        } catch (\Exception $e) {

        return $this->sendError('Server Error',500);

        }
       
       
                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        try {
            $a= Bootcamp::find($id);

        if (!$a) {
            return $this->sendError("bootcamp con id:$id not existe",400);
        }
        $a->delete($id);

        return $this->sendResponse(new BootcampResource($a));

        } catch (\Exception $e) {

            return $this->sendError('Server Error',500);
    
        }
        

        //return response()->json( [ "success"=>true,"data"=>$a ] ,200);
    }
}
