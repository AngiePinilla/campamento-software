<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    //Metodo que controle las reponse exitosas y fallas 

    public function sendResponse($data, $http_status = 200){
        //1. construir la respuesta
        $respuesta = [
            "succes" => true,
            "data" => $data
            
        ];
        //2. enviar response al cliente

        return response()->json($respuesta, $http_status);


    }

    //error

    public function sendError($errors, $http_status = 404){

        //1. construir la respuesta
        $respuesta = [
            "succes" => false,
            "errors" => $errors
            
        ];
        //2. enviar response al cliente

        return response()->json($respuesta, $http_status);

    }
}
